import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'dart:ui' as ui;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:trash_finder/app/trash/details.dart';

class MapsPage extends StatefulWidget {
  @override
  _MapsPageState createState() => _MapsPageState();
}

class _MapsPageState extends State<MapsPage> {
  late GoogleMapController _controller;

  final Stream<QuerySnapshot> _trashStream =
      FirebaseFirestore.instance.collection('trash-list').snapshots();
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  late BitmapDescriptor myIcon;
  late LocationData _currentPosition;
  Location location = Location();
  bool _loading = true;
  List<Marker> customMarkers = [];

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  getLoc() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    _currentPosition = await location.getLocation();
    location.onLocationChanged.listen((LocationData currentLocation) {
      setState(() {
        _currentPosition = currentLocation;
        _loading = false;
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getBytesFromAsset('assets/images/pin.png', 100).then((onValue) {
      myIcon = BitmapDescriptor.fromBytes(onValue);
    });

    getLoc();
  }

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _controller;
    location.onLocationChanged.listen((l) {
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
              target: LatLng(l.latitude ?? 0, l.longitude ?? 0), zoom: 15),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_loading) {
      return Center(
          child: CupertinoActivityIndicator(
        radius: 20,
      ));
    }
    return StreamBuilder<QuerySnapshot>(
      stream: _trashStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Align(
            alignment: Alignment.center,
            child: Center(child: Text("Something went wrong")),
          );
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Align(
            alignment: Alignment.center,
            child: Center(
                child: CupertinoActivityIndicator(
              radius: 20,
            )),
          );
        }

        var list = Future.wait(snapshot.data!.docs.map((e) async {
          Map<String, dynamic> data = e.data()! as Map<String, dynamic>;
          final querySnapshot = await firestore
              .collection('trash-list_users')
              .where('trash-list_id', isEqualTo: data['id'])
              .get();
          double count = 0;
          querySnapshot.docs
              .forEach((value) => {count += value.data()['rating']});
          data['rating'] = count / querySnapshot.docs.length;
          await Geolocator.getCurrentPosition().then((value) {
            data['distance'] = Geolocator.distanceBetween(
                value.latitude, value.longitude, data['lat'], data['long']);
          });
          return data;
        }).toList());

        return FutureBuilder(
          builder: (context, projectSnap) {
            if (projectSnap.data == null) {
              //print('project snapshot data is: ${projectSnap.data}');
              return Align(
                alignment: Alignment.center,
                child: Center(
                    child: CupertinoActivityIndicator(
                  radius: 20,
                )),
              );
            }
            var temp = projectSnap.data as List;
            var markers = [];
            temp.forEach((mark) {
              final Marker marker = Marker(
                icon: myIcon,
                markerId: MarkerId(mark['description']),
                position: LatLng(
                  mark['lat'],
                  mark['long'],
                ),
                infoWindow: InfoWindow(
                    title: mark['rating'] > 0
                        ? '⭐ ' +
                            mark['rating'].toStringAsFixed(1) +
                            ' • ' +
                            mark['type']
                        : mark['type'],
                    snippet: '≅ ' +
                        (mark['distance'] / 1000).toStringAsFixed(1) +
                        ' km',
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (_) => TrashDetailsPage(
                                trash: mark,
                              )));
                    }),
              );
              markers.add(marker);
            });
            return GoogleMap(
                mapType: MapType.normal,
                myLocationEnabled: true,
                markers: markers.cast<Marker>().toSet(),
                initialCameraPosition: CameraPosition(
                    target: LatLng(_currentPosition.latitude ?? 0,
                        _currentPosition.longitude ?? 0),
                    zoom: 15),
                onMapCreated: _onMapCreated);
          },
          future: list,
        );
      },
    );
  }
}
