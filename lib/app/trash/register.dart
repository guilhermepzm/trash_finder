import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:select_form_field/select_form_field.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  var _formKey = GlobalKey<FormState>();

  String? description, lat, long, type;

  final List<Map<String, dynamic>> _items = [
    {
      'value': 'Lixeira Regular',
      'label': 'Lixeira Regular',
      'icon': Icon(
        CupertinoIcons.trash_fill,
        color: Colors.green[400],
      ),
    },
    {
      'value': 'Lixeira Reciclável',
      'label': 'Lixeira Reciclável',
      'icon': Icon(
        CupertinoIcons.trash_fill,
        color: Colors.green[400],
      ),
    },
    {
      'value': 'Ponto de descarte de material reciclável',
      'label': 'Ponto de descarte de material reciclável',
      'icon': Icon(
        CupertinoIcons.house_fill,
        color: Colors.green[400],
      ),
    },
    {
      'value': 'Ponto de descarte de materiais eletrônicos',
      'label': 'Ponto de descarte de materiais eletrônicos',
      'icon': Icon(
        CupertinoIcons.house_fill,
        color: Colors.green[400],
      ),
    },
    {
      'value': 'Ponto de descarte de medicamentos',
      'label': 'Ponto de descarte de medicamentos',
      'icon': Icon(
        CupertinoIcons.house_fill,
        color: Colors.green[400],
      ),
    },
    {
      'value': 'Ponto de descarte de óleo de cozinha',
      'label': 'Ponto de descarte de óleo de cozinha',
      'icon': Icon(
        CupertinoIcons.house_fill,
        color: Colors.green[400],
      ),
    },
  ];

  final _controllerLat = TextEditingController();
  final _controllerLong = TextEditingController();
  final _controllerType = TextEditingController();

  @override
  void dispose() {
    // other dispose methods
    _controllerLat.dispose();
    _controllerLong.dispose();
    super.dispose();
  }

  Future _register(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      try {
        firestore.collection('trash-list').add({
          'description': description!.trim(),
          'lat': double.parse(lat ?? '0'),
          'long': double.parse(long ?? '0'),
          'created_at': DateTime.now(),
          "type": type!.trim(),
        }).then((docRef) => {
              FirebaseFirestore.instance
                  .collection('trash-list')
                  .doc(docRef.id)
                  .update({'id': docRef.id})
            });
        Navigator.pop(context);
      } on FirebaseAuthException catch (ex) {
        print(ex.message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Geolocator.getCurrentPosition().then((value) {
      setState(() {
        _controllerLat.text = value.latitude.toString();
        _controllerLong.text = value.longitude.toString();
      });
    });

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.green[400],
        title: Text("Registro"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Descrição",
                ),
                onSaved: (value) => description = value,
                validator: (value) {
                  if (value!.length == 0)
                    return "Por favor, insira uma descrição";
                  return null;
                },
              ),
              TextFormField(
                controller: _controllerLat,
                decoration: InputDecoration(
                  labelText: "Latitude",
                ),
                onSaved: (value) => lat = value,
                validator: (value) {
                  if (value!.length == 0) return "Campo requirido";
                  return null;
                },
              ),
              TextFormField(
                controller: _controllerLong,
                decoration: InputDecoration(
                  labelText: "Logintude",
                ),
                onSaved: (value) => long = value,
                validator: (value) {
                  if (value!.length == 0) return "Campo requirido";
                  return null;
                },
              ),
              SelectFormField(
                type: SelectFormFieldType.dialog,
                dialogTitle: 'Tipos',
                controller: _controllerType,
                decoration: InputDecoration(
                  labelText: "Tipo",
                ),
                items: _items,
                onSaved: (value) => type = value,
                validator: (value) {
                  if (value!.length == 0) return "Por favor, insira um tipo";
                  return null;
                },
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.green[400]),
                  onPressed: () => _register(context),
                  child: Text("Salvar"),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
