import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:maps_launcher/maps_launcher.dart';

class TrashDetailsPage extends StatefulWidget {
  const TrashDetailsPage({Key? key, required this.trash}) : super(key: key);

  // Declare a field that holds the Todo.
  final Map trash;
  @override
  _TrashDetailsPageState createState() => _TrashDetailsPageState(trash);
}

class _TrashDetailsPageState extends State<TrashDetailsPage> {
  _TrashDetailsPageState(this.trash);
  final FirebaseAuth auth = FirebaseAuth.instance;
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  var _formKey = GlobalKey<FormState>();
  String? description;
  double rating = 5;

  final Map trash;

  void initState() {
    // TODO: implement initState
    super.initState();

    firestore
        .collection('trash-list_users')
        .where('users_id', isEqualTo: auth.currentUser?.uid)
        .where('trash-list_id', isEqualTo: trash['id'])
        .get()
        .then((querySnapshot) => {
              setState(() {
                rating = querySnapshot.docs.first.data()['rating'];
              })
            });
  }

  Future _register(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      try {
        final querySnapshot = await firestore
            .collection('trash-list_users')
            .where('users_id', isEqualTo: auth.currentUser?.uid)
            .where('trash-list_id', isEqualTo: trash['id'])
            .get();

        if (querySnapshot.docs.isEmpty) {
          firestore.collection('trash-list_users').add({
            'description': description,
            'rating': rating,
            'created_at': DateTime.now(),
            'users_id': auth.currentUser?.uid,
            'trash-list_id': trash['id']
          }).then((docRef) => {
                FirebaseFirestore.instance
                    .collection('trash-list_users')
                    .doc(docRef.id)
                    .update({'id': docRef.id})
              });
        } else {
          FirebaseFirestore.instance
              .collection('trash-list_users')
              .doc(querySnapshot.docs.first.data()['id'])
              .update({
            'description': description,
            'rating': rating,
            'created_at': DateTime.now(),
          });
        }
        final snackBar = SnackBar(
          backgroundColor: Colors.green,
          elevation: 6.0,
          behavior: SnackBarBehavior.floating,
          margin: EdgeInsets.fromLTRB(10, 0, 10, 20),
          duration: Duration(seconds: 2),
          content: const Text('Muito obrigado pela avaliação! 🥳'),
          action: SnackBarAction(
            label: 'Fechar',
            textColor: Colors.white,
            onPressed: () {
              // Some code to undo the change.
            },
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } on FirebaseAuthException catch (ex) {
        print(ex.message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.green[400],
        title: Text("Detalhes"),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Card(
              shadowColor: Colors.green[400],
              margin: new EdgeInsets.only(bottom: 20),
              child: InkWell(
                splashColor: Colors.green[400],
                child: SizedBox(
                  width: double.infinity,
                  child: Column(
                    children: [
                      ListTile(
                        leading: Icon(
                          trash['type'].contains('Lixeira')
                              ? CupertinoIcons.trash_fill
                              : CupertinoIcons.house_fill,
                          color: Colors.green[400],
                          size: 36,
                        ),
                        title: Text(trash['description']),
                        subtitle: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Text(
                                trash['rating'] > 0
                                    ? '⭐ ' +
                                        trash['rating'].toStringAsFixed(1) +
                                        ' • ' +
                                        trash['type']
                                    : trash['type'],
                                maxLines: 2,
                                style: TextStyle(
                                    color: Colors.black.withOpacity(0.6)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                (trash['distance'] / 1000).toStringAsFixed(1) +
                                    ' km',
                                style: TextStyle(
                                    color: Colors.black.withOpacity(0.6)),
                              ),
                            ),
                          ],
                        ),
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.center,
                        children: [
                          TextButton.icon(
                            onPressed: () {
                              MapsLauncher.launchCoordinates(
                                  trash['lat'], trash['long']);
                            },
                            icon: Icon(
                              Icons.map_sharp,
                              color: Colors.green[400],
                            ),
                            label: const Text(
                              'ABRIR NO MAPS',
                              style: TextStyle(color: Colors.green),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            trash['distance'] < 200
                ? Card(
                    shadowColor: Colors.green[400],
                    margin: new EdgeInsets.only(bottom: 20),
                    child: InkWell(
                      splashColor: Colors.green[400],
                      child: Form(
                        key: _formKey,
                        child: SizedBox(
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(children: [
                              Text(
                                "Avaliação",
                                style: TextStyle(
                                    fontSize: 20, color: Colors.green[400]),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(5, 10, 5, 10),
                                child: Text(
                                  "Por favor, nos ajude a manter a qualidade de nossas indicações avaliando a qualidade deste " +
                                      trash['type'].toString().toLowerCase(),
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black.withOpacity(0.6)),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              RatingBar.builder(
                                initialRating: rating,
                                minRating: 0,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemPadding:
                                    EdgeInsets.symmetric(horizontal: 4.0),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (value) {
                                  setState(() {
                                    rating = value;
                                  });
                                },
                              ),
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: TextFormField(
                                  decoration: InputDecoration(
                                    labelText: "Descrição",
                                  ),
                                  onSaved: (value) => description = value,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(5),
                                child: Container(
                                  width: double.infinity,
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        primary: Colors.green[400]),
                                    onPressed: () => _register(context),
                                    child: Text("Avaliar"),
                                  ),
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),
                    ),
                  )
                : Text('')
          ],
        ),
      ),
    );
  }
}
