import 'package:filter_list/filter_list.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:trash_finder/app/configurations/configurations.dart';
import 'package:trash_finder/app/maps/maps.dart';
import 'package:flutter/material.dart';
import 'package:trash_finder/app/trash/add-button.dart';
import 'package:trash_finder/app/trash/list.dart';
import 'package:trash_finder/auth/login.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  int _selectedIndex = 1;

  Future _logout(BuildContext context) async {
    await auth.signOut();
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => LoginPage()), (route) => false);
  }

  List<String> _selectedFilter = [
    'Lixeira Regular',
    'Lixeira Reciclável',
    'Ponto de descarte de material reciclável',
    'Ponto de descarte de materiais eletrônicos',
    'Ponto de descarte de medicamentos',
    'Ponto de descarte de óleo de cozinha'
  ];

  List<String> _filterOptions = [
    'Lixeira Regular',
    'Lixeira Reciclável',
    'Ponto de descarte de material reciclável',
    'Ponto de descarte de materiais eletrônicos',
    'Ponto de descarte de medicamentos',
    'Ponto de descarte de óleo de cozinha'
  ];

  void openFilterDelegate() async {
    await FilterListDelegate.show<String>(
      context: context,
      list: _filterOptions,
      selectedListData: _selectedFilter,
      onItemSearch: (option, query) {
        return option.toLowerCase().contains(query.toLowerCase());
      },
      tileLabel: (option) => option,
      emptySearchChild: Center(child: Text('Nenhum tipo encontrado')),
      searchFieldHint: 'Pesquisar...',
      onApplyButtonClick: (list) {
        setState(() {
          _selectedFilter = List.from(list!);
        });
      },
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      if (index == 2) {
        showModalBottomSheet(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            ),
            context: context,
            builder: (context) {
              return Container(
                alignment: Alignment.center,
                height: 150,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Realmente deseja sair?',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 24),
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Não",
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 18,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            _logout(context);
                          },
                          child: Text(
                            "Sim",
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: 18,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              );
            });
        return;
      }
      _selectedIndex = index;
    });
  }

  returnBody(currentIndex) {
    List<Widget> widgetOptions = <Widget>[
      TrashListPage(
        filter: _selectedFilter,
      ),
      MapsPage(),
      ConfigurationsPage()
    ];
    return widgetOptions[currentIndex];
  }

  static List<Widget> _widgetTitles = <Widget>[
    Text(
      'Pontos de Descarte',
    ),
    Text(
      'Home',
    ),
    Text(
      'Configurações',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[400],
        title: _widgetTitles.elementAt(_selectedIndex),
        actions: _selectedIndex == 0
            ? [
                IconButton(
                  icon: Icon(
                    Icons.filter_list,
                  ),
                  onPressed: () {
                    openFilterDelegate();
                  },
                ),
              ]
            : [],
      ),
      body: returnBody(_selectedIndex),
      floatingActionButton: Visibility(
        visible: _selectedIndex == 0,
        child: AddButtonPage(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.trash_fill),
            label: 'Pontos de\n Descarte',
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.house_fill),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.logout),
            label: 'Sair',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        backgroundColor: Colors.green[400],
        onTap: _onItemTapped,
      ),
    );
  }
}
