import 'package:trash_finder/auth/register.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../app/home.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  var _formKey = GlobalKey<FormState>();

  String? email, senha;
  bool _passwordVisible = false;
  Future _login(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      try {
        await auth.signInWithEmailAndPassword(
            email: email!.trim(), password: senha!.trim());

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (_) => HomePage()), (route) => false);
      } on FirebaseAuthException catch (ex) {
        print(ex.message);
        final snackBar = SnackBar(
          backgroundColor: Colors.red,
          elevation: 6.0,
          behavior: SnackBarBehavior.floating,
          margin: EdgeInsets.fromLTRB(10, 0, 10, 20),
          duration: Duration(seconds: 2),
          content: const Text('E-mail ou senha inválidos! 😕'),
          action: SnackBarAction(
            label: 'Fechar',
            textColor: Colors.white,
            onPressed: () {
              // Some code to undo the change.
            },
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[400],
        title: Text("Login"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: const EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: Container(
                      width: 120,
                      height: 120,
                      child: Image.asset('assets/images/logo.png')),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 140),
                  child: Column(
                    children: [
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: "E-mail",
                        ),
                        onSaved: (value) => email = value,
                        validator: (value) {
                          if (value!.length == 0)
                            return "Por favor, insira seu E-mail";
                          return null;
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: "Senha",
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              _passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Theme.of(context).primaryColorDark,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            },
                          ),
                        ),
                        obscureText: !_passwordVisible,
                        onSaved: (value) => senha = value,
                        validator: (value) {
                          if (value!.length == 0)
                            return "Por favor, insira sua senha";
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: double.infinity,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.green[400]),
                          onPressed: () => _login(context),
                          child: Text("Entrar"),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextButton(
                        style: TextButton.styleFrom(primary: Colors.green[400]),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (_) => RegisterPage()));
                        },
                        child: Text("Não tem cadastro, clique aqui."),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
