import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:flutter/material.dart';

import '../app/home.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  var _formKey = GlobalKey<FormState>();

  String? nome, email, senha, telefone, dataNasc;
  bool _passwordVisible = false;

  final phoneMask = new MaskTextInputFormatter(
      mask: '(##) #########',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);

  final birthDateMask = new MaskTextInputFormatter(
      mask: '##/##/####',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);

  Future _register(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      try {
        await auth.createUserWithEmailAndPassword(
            email: email!.trim(), password: senha!.trim());

        await auth.currentUser?.updateDisplayName(nome);

        // Adiciona o apelido no firestore (database):
        firestore.collection('users').doc(auth.currentUser?.uid).set({
          'telefone': telefone!.trim(),
          'dataNasc': dataNasc!.trim(),
          'data': DateTime.now(),
          'id': auth.currentUser?.uid
        });

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (_) => HomePage()), (route) => false);
      } on FirebaseAuthException catch (ex) {
        print(ex.message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Registro"),
        backgroundColor: Colors.green[400],
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Nome",
                ),
                onSaved: (value) => nome = value,
                validator: (value) {
                  if (value!.length == 0) return "Por favor, insira um nome";
                  return null;
                },
              ),
              TextFormField(
                inputFormatters: [phoneMask],
                decoration: InputDecoration(
                  labelText: "Telefone",
                ),
                onSaved: (value) => telefone = value,
                validator: (value) {
                  if (value!.length == 0)
                    return "Por favor, insira um telefone";
                  if (value.length != 14 && value.length != 13)
                    return "Por favor, insira um telefone válido";
                  return null;
                },
              ),
              TextFormField(
                inputFormatters: [birthDateMask],
                decoration: InputDecoration(
                  labelText: "Data de nascimento",
                ),
                onSaved: (value) => dataNasc = value,
                validator: (value) {
                  if (value!.length == 0)
                    return "Por favor, insira uma data de nascimento";
                  if (value.length != 10)
                    return "Por favor, insira uma data de nascimento válida";
                  return null;
                },
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: "E-mail",
                ),
                onSaved: (value) => email = value,
                validator: (value) {
                  if (value!.length == 0) return "Por favor, insira um E-mail";
                  if (!RegExp(
                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                      .hasMatch(value))
                    return "Por favor, insira um E-mail válido";
                  return null;
                },
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Senha",
                  suffixIcon: IconButton(
                    icon: Icon(
                      // Based on passwordVisible state choose the icon
                      _passwordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: Theme.of(context).primaryColorDark,
                    ),
                    onPressed: () {
                      // Update the state i.e. toogle the state of passwordVisible variable
                      setState(() {
                        _passwordVisible = !_passwordVisible;
                      });
                    },
                  ),
                ),
                obscureText: !_passwordVisible,
                onSaved: (value) => senha = value,
                validator: (value) {
                  if (value!.length == 0) return "Por favor, insira uma senha";
                  return null;
                },
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.green[400]),
                  onPressed: () => _register(context),
                  child: Text("Registrar"),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
